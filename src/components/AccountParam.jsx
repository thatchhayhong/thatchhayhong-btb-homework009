import React from 'react';
import { Container } from 'react-bootstrap';
import { useParams } from 'react-router'

function AccountParam() {
    let { accountID } = useParams();
    console.log(accountID)
    return (
        <Container>
      <div>
        <h3>{accountID}</h3>
      </div>
        </Container>

    );
}

export default AccountParam;
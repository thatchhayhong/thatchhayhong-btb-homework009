import { Container } from 'react-bootstrap'
import AccountParam from './AccountParam';
import {
    Switch,
    Route,
    useRouteMatch,
    NavLink
  } from "react-router-dom";

function AccountNestComponent(props) {
    let { path, url } = useRouteMatch();
    console.log("Path: ",path,"Url: ",url);
  return (
        <Container>
          <Switch>
        <Route exact path={path}>
          <h5>Please select an account</h5>
          <ul>
              {props.account.data.map((item,index)=>(
                  <li key={index}><NavLink to={`${url}/${item}`}>{item}</NavLink></li>
              ))}
          </ul>
          <br></br>
        </Route>
        <Route exact path={`${path}/:accountID`}>
          <AccountParam />
        </Route>
      </Switch>
        </Container>
  );
}
export default AccountNestComponent;
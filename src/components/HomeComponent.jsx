import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import CardComponent from "./CardComponent";
import {  useHistory } from 'react-router'

function Home() {
  const [movie, setMovie] = useState({
    data: [
      {
        id: 1,
        title: "Clever Boy",
        description: "Contents",
        movieImg:
          "https://cdn.lynda.com/course/2841398/2841398-637490964925047228-16x9.jpg",
      },
      {
        id: 2,
        title: "Clever Boy",
        description: "Contents",
        movieImg: "https://i.gifer.com/HoGQ.gif",
      },
      {
        id: 3,
        title: "Clever Boy",
        description: "Contents",
        movieImg:
          "https://helpx.adobe.com/content/dam/help/en/mobile-apps/how-to/animate-ink-drawing-adobe-capture/_jcr_content/main-pars/image_635913194/animate-ink-drawing-adobe-capture_1408x792.jpg",
      },
      {
        id: 4,
        title: "Clever Boy",
        description: "Contents",
        movieImg:
          "https://www.pngarea.com/pngs/114/7473512_monsters-inc-png-animated-monster-clipart-png-download.png",
      },
      {
        id: 5,
        title: "Clever Boy",
        description: "Contents",
        movieImg:
          "https://www.pngitem.com/pimgs/m/571-5710738_cartoon-baby-lion-clipart-png-download-transparent-background.png",
      },
    ],
  });
  const history = useHistory();
  const onViewDetail=(id)=>{
      let path = `/view/${id}`; 
      let selectedMovie=movie.data.filter((item)=>item.id===id)
      console.log(selectedMovie)
      history.push(path,selectedMovie);
  }
  return (
    <div>
      <Container>
      <Col >
        <Row xs={1} md={2} lg={4}>
            <CardComponent data={movie.data} onViewDetail={onViewDetail}/>
        </Row>
        </Col>
      </Container>
    </div>
  );
}

export default Home;

import React from "react";
import { useState } from "react";
import { Container } from "react-bootstrap";
import VideoNestComponent from "./VideoNestComponent";
import {
  Switch,
  Route,
  useRouteMatch,
  NavLink
} from "react-router-dom";

function VideoComponent() {
  let { path, url } = useRouteMatch();
  const [video, setVideo] = useState({
    data: [
      { id: 1, title: "Bahubali", category: "Action" },
      { id: 2, title: "The Jungle", category: "Cartoon" },
      { id: 3, title: "Two Love", category: "Dramma" },
      { id: 4, title: "The War Hero", category: "Action" },
      { id: 5, title: "Soul", category: "Cartoon" },
    ],
    category: ["Cartoon", "Action", "Dramma"],
    filter:[]
  });
    const getMovieByCateogory=(category)=>{
      let filter=video.data.filter((item)=>item.category===category)
      setVideo((prevState) => ({
        ...prevState,
       filter:filter
      }));
    }
  return (
    <>
      <Container>
          <Switch>
        <Route exact path={path}>
          <h5>Categories</h5>
          <ul>
              {video.category.map((item,index)=>(
                  <li key={index}><NavLink to={`${url}/${item}`}><span onClick={()=>getMovieByCateogory(item)}>{item}</span></NavLink></li>
              ))}
          </ul>
          <br></br>
            
        </Route>
        <Route exact path={`${path}/:movieCategory`}>
          <VideoNestComponent data={video.filter}/>
        </Route>
      </Switch>
        </Container>
    </>
  );
}

export default VideoComponent;

import React, { useState } from "react";
import { Container } from "react-bootstrap";
import AccountNestComponent from "./AccountNestComponent";
import {
  Route,
  useRouteMatch,
  NavLink
} from "react-router-dom";
import AccountParam from "./AccountParam";
function AccountComponent() {
  let { path, url } = useRouteMatch();
  const [account, setAccount] = useState({
    data: ["Netflix", "Youtube", "Yahoo", "TikTok"],
  });
  return (
    <>
      <Container>
        {/* <AccountNestComponent account={account}/> */}
        <h5>Account</h5>
        <ul>
              {account.data.map((item,index)=>(
                  <li key={index}><NavLink to={`${url}/${item}`}>{item}</NavLink></li>
              ))}
          </ul>
      </Container>
      <Route exact path={`${path}/:accountID`}>
          <AccountParam />
        </Route>
    </>
  );
}

export default AccountComponent;

import React from "react";
import { Card, Button, } from "react-bootstrap";
function CardComponent(props) {
  return (
    <>
      {props.data.map((item, index) => (
        <Card key={item.id} className="mt-3">
          <Card.Img variant="top" src={item.movieImg} height="200"/>
          <Card.Body>
            <Card.Title>#{item.id} {item.title}</Card.Title>
            <Card.Text>{item.description}</Card.Text>
            <Button variant="primary" onClick={()=>props.onViewDetail(item.id)}>Read</Button>
          </Card.Body>
        </Card>
      ))}
    </>
  );
}

export default CardComponent;

import React from "react";
import { Card,Container } from "react-bootstrap";
import { useParams,useLocation } from 'react-router'
import { NavLink } from 'react-router-dom'

function CardDetail() {
    let {state}=useLocation()
    let { id } = useParams()
    console.log(state)
  return (
    <>
    <Container>
    <h3>ID: {id}</h3>
      {state?
        <Card key={id}>
          <Card.Img variant="top" src={state[0].movieImg}/>
          <Card.Body>
            <Card.Title>{state[0].title}</Card.Title>
            <Card.Text>{state[0].description}</Card.Text>
          </Card.Body>
        </Card>:<div>
        <h3>No data found!</h3>
        <br></br>
        <NavLink to='/'>Go back</NavLink>
        </div>
      }
    </Container>
     
    </>
  );
}

export default CardDetail;

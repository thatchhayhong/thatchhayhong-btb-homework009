import React, { useState } from 'react';
import {
    Route,
    useRouteMatch,
    NavLink
  } from "react-router-dom";
import {  useHistory } from 'react-router'

function VideoNestComponent(props) {
   const [watchMovie,setMovie]=useState({
      data:[]
   })
    let {path,url} = useRouteMatch();
    const history = useHistory();
    const onViewDetail=(item)=>{
        let path = url; 
        history.push(path,item);
        setMovie((prevState) => ({
         ...prevState,
         data:item
       }));
    }

    console.log(path,url)
  return (
     <>
     <h4>{props?.data[0]?.category}</h4>
     <ul>
     {props.data.map((item,index)=>(<>
        <li  key={index}>
        {/* <span onClick={()=>onViewDetail(item)}>{item.title}</span> */}
        <NavLink to={`${url}}`}><span onClick={()=>onViewDetail(item)}>{item.title}</span></NavLink>
        </li>
     </>))}
      <h4>Watching: {watchMovie.data.title}</h4>
     </ul>
        {/* <Route exact path={`${path}/:movie`}>
          {console.log(watchMovie)}
        </Route> */}
     </>
  );
}

export default VideoNestComponent;
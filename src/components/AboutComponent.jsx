import React from 'react';
import { Container } from 'react-bootstrap';
function AboutComponent() {
  return (
    <Container>
      <div>
         <h3> About Component</h3>
      </div>
    </Container>
  );
}

export default AboutComponent;
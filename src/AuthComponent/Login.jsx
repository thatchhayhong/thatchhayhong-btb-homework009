import React from "react";
import { Container, Button,Form } from "react-bootstrap";
import {useHistory } from "react-router";
import Auth from "./Auth";
function Login() {
  let history = useHistory();
  console.log(Auth.isAuth);
  return (
    <Container>
      <h1>{Auth.isAuth ? "Logout" : "Login"}</h1>
      {!Auth.isAuth ? (
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" name="password" autoComplete="on" placeholder="Password" />
          </Form.Group>
        </Form>
      ) : (
        ""
      )}
      {Auth.isAuth ? (
        <Button
          onClick={() => {
            Auth.logout(() => {
              history.push("/login");
            });
          }}
        >
          Logout
        </Button>
      ) : (
        <Button
          onClick={() => {
            Auth.login(() => {
              history.push("/");
            });
          }}
        >
          Login
        </Button>
      )}
    </Container>
  );
}

export default Login;

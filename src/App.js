import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from './components/HomeComponent'
import AboutComponent from './components/AboutComponent';
import AuthGuard from './AuthComponent/AuthGuard';
import MyNavComponent from './components/MyNavComponent';
import {Container} from 'react-bootstrap';
import Login from './AuthComponent/Login'
import CardDetail from './components/CardDetail';
import VideoComponent from './components/VideoComponent';
import AccountComponent from './components/AccountComponent';
function App() {
  return (
    <>
    <Router>
      <MyNavComponent/>
      <Switch className="mt-3">
        <AuthGuard exact path='/'>
          <Home />
        </AuthGuard>
        <AuthGuard exact path='/about'>
          <AboutComponent />
        </AuthGuard>
        <AuthGuard  path='/video'>
        <VideoComponent/>
        </AuthGuard>
        <AuthGuard  path='/account'>
        <AccountComponent/>
        </AuthGuard>
        <Route path='/view/:id' render={() =><CardDetail/>} />
        <Route path='/video/:id' render={()=><VideoComponent/>}/>
        <Route path='/login' render={() => <Login />} />
        <Route path='*' render={(props) => <Container><h1>404 Page Not Found</h1></Container>} />
      </Switch>
    </Router>
  </>
  );
}

export default App;
